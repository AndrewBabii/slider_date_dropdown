import 'package:flutter/material.dart';
import 'package:slider_date_dropdown/data_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double sliderValue = 0.0;

  String gender = 'unpicked';

  TimeOfDay time = TimeOfDay.now();

  DateTime date = DateTime.now();

  final List<String> gendersList = ['male', 'female'];

  void saveData() {
    DataModel dm = DataModel(
        date: date, time: time, gender: gender, sliderValue: sliderValue);
    dm.toString();
  }

  void _presentDataPicker(/*BuildContext context*/) {
    print('presentTime');
    showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(1980),
            lastDate: DateTime.now())
        .then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      date = pickedDate;
    });
  }

  void _presentTimePicker(/*BuildContext context*/) {
    print('presentDate');
    showTimePicker(context: context, initialTime: TimeOfDay.now())
        .then((pickedTime) {
      if (pickedTime == null) {
        return;
      }
      time = pickedTime;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('title'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            IconButton(
                icon: Icon(Icons.calendar_today),
                onPressed:
                    _presentDataPicker /* () {
                _presentDataPicker(context);
              },*/
                ),
            IconButton(
                icon: Icon(Icons.access_time),
                onPressed:
                    _presentTimePicker /*() {
                _presentTimePicker(context);
              },*/
                ),
            DropdownButton<String>(
                items: gendersList.map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: new Text(value),
                  );
                }).toList(),
                onChanged: (newVal) {
                  print('onChanged');
                  gender = newVal;
                }),
            Slider(
                value: sliderValue,
                onChanged: (newRating) {
                  setState(() => sliderValue = newRating);
                },),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: saveData,
        child: Icon(Icons.add),
      ),
    );
  }
}
