import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DataModel {
  DateTime date;
  TimeOfDay time;
  String gender;
  double sliderValue;

  DataModel({this.date, this.time, this.gender, this.sliderValue});

  @override
  String toString() {
    String s = ('you pick this date = ${DateFormat.yMd().format(date)}, time = $time, gender = $gender, slider = $sliderValue');
    print (s);
        return s;
  }
}


